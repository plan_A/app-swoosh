//
//  Player.swift
//  app-swoosh
//
//  Created by Jenia on 5/23/18.
//  Copyright © 2018 Jenia. All rights reserved.
//

import Foundation

struct Player {
    var desiredLeague: String?
    var selectedSkillLevel: String?    
}
